from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    return render(request, 'books/index.html')


def contact(request):
    return render(request, 'books/kontakt.html')
